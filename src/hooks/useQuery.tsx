import {useCallback, useEffect, useRef, useState} from 'react';
import {AxiosError, AxiosRequestConfig} from 'axios';
import axios, {isCancel} from './../core/lib/axios';
import toast from 'react-hot-toast';
import {IApiResultModel} from './../core/models/api.models';

export interface IQueryParamsModel {
  page?: number;
  page_size?: number;
  lat: number;
  long: number;
}

export interface ICoordinateModel {
  lat: number;
  long: number;
}

interface IApiCallbackModel<T> {
  onSuccess?: (data: IApiResultModel<T>) => void;
  onFail?: (data: IApiResultModel<T>) => void;
  onError?: (data: AxiosError) => void;
  onFinally?: () => void;
}

const useAxios = <T extends unknown>({
  url = '',
  pageNumber,
  pageSize = 10,
  coordinate = {lat: 35.754, long: 51.328},
  filters,
  callback,
}: {
  url?: string;
  pageNumber?: number;
  pageSize?: number;
  coordinate?: ICoordinateModel;
  filters?: Array<string>;
  callback?: IApiCallbackModel<T>;
}) => {
  const [data, setData] = useState<IApiResultModel<T> | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<AxiosError | null>(null);
  const [isUnmount, setIsUnmount] = useState<boolean>(false);

  const sendRequest = useCallback(
    (dynamicPageNumber?: number): Promise<void> => {
      const requestUrl = url;
      const config: AxiosRequestConfig = {
        params: {
          page: dynamicPageNumber || pageNumber,
          page_size: pageSize,
          lat: coordinate?.lat,
          long: coordinate?.long,
          filters: {
            filters: filters,
          },
        },
      };
      setIsLoading(true);
      return axios
        .get<IApiResultModel<T>>(requestUrl, config)
        .then(response => {
          if (isUnmount) return;
          setData(response.data);
          if (response.data?.status) {
            callback?.onSuccess?.(response.data);
          } else {
            if (typeof callback?.onFail === 'function') {
              callback?.onFail?.(response.data);
            } else {
              let errorMessage = 'خطایی رخ داده است!';
              toast.error(errorMessage);
            }
          }
        })
        .catch(error => {
          if (isUnmount) return;
          if (isCancel(error)) return;
          setError(error);
          if (typeof callback?.onError === 'function') {
            callback?.onError?.(error);
          } else {
            let errorMessage = error?.message || 'خطایی رخ داده است!';
            toast.error(errorMessage);
          }
        })
        .finally(() => {
          if (isUnmount) return;
          setIsLoading(false);
          callback?.onFinally?.();
        });
    },
    [url, callback, isUnmount, pageNumber, pageSize, filters, coordinate],
  );

  useEffect(() => {
    sendRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    return () => {
      setIsUnmount(true);
    };
  }, []);

  return {
    sendRequest,
    data,
    isLoading,
    error,
  };
};

export default useAxios;
