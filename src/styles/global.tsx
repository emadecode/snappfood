import {createGlobalStyle} from 'styled-components';
import {Theme} from './theme';
import {vazirFont} from './fonts';

export type IPropsModel = {
  theme: Theme;
};

const GlobalStyle = createGlobalStyle`
          ${vazirFont}
          * {
            font-family: "Vazir";
          }
          p {
            margin-top: 0;
            margin-bottom: 1rem;
          }

          .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
          margin-bottom: 0.5rem;
          font-family: inherit;
          font-weight: 500;
          line-height: 1.2;
          color: inherit;
          }
          html {
            font-size: 62.5%;
            @media only screen and (min-width: 720px) {
              font-size: 87.5%;
            }
          }
          body {
            font-family: "Vazir";
            text-align: right;
          }
          ::-webkit-scrollbar {
            width: 1rem;
            height: 0.5rem;
          }

          /* Track */
          ::-webkit-scrollbar-track {
            background: #f1f1f1;
          }

          /* Handle */
          ::-webkit-scrollbar-thumb {
            background-color: #a8bbbf;
            border-radius: 3rem;
          }

          /* Handle on hover */
          ::-webkit-scrollbar-thumb:hover {
            background-color: #9eb2b7;
          }
`;

export default GlobalStyle;
