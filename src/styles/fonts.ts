import {css} from 'styled-components';

export const vazirFont = css`
  @font-face {
    font-family: Vazir;
    src: url('../fonts/vazir-font/farsi-digits/Vazir-FD.eot');
    src: url('../fonts/vazir-font/farsi-digits/Vazir-FD.eot?#iefix')
        format('embedded-opentype'),
      url('../fonts/vazir-font/farsi-digits/Vazir-FD.woff2') format('woff2'),
      url('../fonts/vazir-font/farsi-digits/Vazir-FD.woff') format('woff'),
      url('../fonts/vazir-font/farsi-digits/Vazir-FD.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: Vazir;
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Bold-FD.eot');
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Bold-FD.eot?#iefix')
        format('embedded-opentype'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Bold-FD.woff2')
        format('woff2'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Bold-FD.woff') format('woff'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Bold-FD.ttf')
        format('truetype');
    font-weight: bold;
    font-style: normal;
  }

  @font-face {
    font-family: Vazir;
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Black-FD.eot');
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Black-FD.eot?#iefix')
        format('embedded-opentype'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Black-FD.woff2')
        format('woff2'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Black-FD.woff') format('woff'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Black-FD.ttf')
        format('truetype');
    font-weight: 900;
    font-style: normal;
  }

  @font-face {
    font-family: Vazir;
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Medium-FD.eot');
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Medium-FD.eot?#iefix')
        format('embedded-opentype'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Medium-FD.woff2')
        format('woff2'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Medium-FD.woff')
        format('woff'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Medium-FD.ttf')
        format('truetype');
    font-weight: 500;
    font-style: normal;
  }

  @font-face {
    font-family: Vazir;
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Light-FD.eot');
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Light-FD.eot?#iefix')
        format('embedded-opentype'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Light-FD.woff2')
        format('woff2'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Light-FD.woff') format('woff'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Light-FD.ttf')
        format('truetype');
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: Vazir;
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Thin-FD.eot');
    src: url('../fonts/vazir-font/farsi-digits/Vazir-Thin-FD.eot?#iefix')
        format('embedded-opentype'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Thin-FD.woff2')
        format('woff2'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Thin-FD.woff') format('woff'),
      url('../fonts/vazir-font/farsi-digits/Vazir-Thin-FD.ttf')
        format('truetype');
    font-weight: 100;
    font-style: normal;
  }

  @font-face {
    font-family: Tanha;
    src: url('../fonts/tanha-font/farsi-digits/Tanha-FD.eot');
    src: url('../fonts/tanha-font/farsi-digits/Tanha-FD.eot?#iefix')
        format('embedded-opentype'),
      url('../fonts/tanha-font/farsi-digits/Tanha-FD.woff') format('woff'),
      url('../fonts/tanha-font/farsi-digits/Tanha-FD.ttf') format('truetype');
    font-weight: normal;
  }
`;
