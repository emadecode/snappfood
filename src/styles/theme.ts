import {defaultColors} from './colors';

const APP_THEME = {
  main: 'snappTheme',
  palette: {
    ...defaultColors,
  },
};

export type Theme = typeof APP_THEME;

export default APP_THEME;

export interface IStyledThemeProps {
  theme?: Theme;
}
