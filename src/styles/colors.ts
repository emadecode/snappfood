export const defaultColors = {
  primary: {
    main: '#22a958',
    light: '#25c364',
    dark: '#082032',
  },
  secondary: {
    main: '#ff00a4',
    light: '#ef4136',
    dark: '#2C394B',
  },
  error: {
    main: '#D8000C',
    light: '#D8000C',
    dark: '#D8000C',
  },
  background: {
    default: '#F3F3F3',
  },
};
