import React from 'react';
import styles from './App.module.scss';
import AppRoutes from './AppRoutes';
import {ThemeProvider} from 'styled-components';
import APP_THEME from './styles/theme';
import GlobalStyle from './styles/global';

function App() {
  return (
    <ThemeProvider theme={APP_THEME}>
      <GlobalStyle />
      <AppRoutes />
    </ThemeProvider>
  );
}

export default App;
