import {
  ITextItemTypeModel,
  IVendorItemModel,
} from '../areas/vendors-list-page/VendorsListPage.models';

export function isVendorItem(
  argument: IVendorItemModel | ITextItemTypeModel,
): argument is IVendorItemModel {
  return argument.type === 'VENDOR';
}
