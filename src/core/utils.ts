export const numberTocurrency = (number: number): string => number.toFixed(2);
