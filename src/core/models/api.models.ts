export interface IApiResultModel<T> {
  status: boolean;
  data: T;
}
