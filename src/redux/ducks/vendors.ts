import {createAction, createReducer} from '@reduxjs/toolkit';
import {
  IFilterItemModel,
  IVendorItemModel,
} from '../../areas/vendors-list-page/VendorsListPage.models';

const ADD_VENDORS_LIST = 'snappFood/vendors/ADD_VENDORS_LIST';
const CLEAR_VENDORS_LIST = 'snappFood/vendors/CLEAR_VENDORS_LIST';
const ADD_SELECTED_FILTER = 'snappFood/vendors/ADD_SELECTED_FILTER';
const REMOVE_SELECTED_FILTER = 'snappFood/vendors/REMOVE_SELECTED_FILTER';
const ADD_VENDORS_FILTERS = 'snappFood/vendors/ADD_VENDORS_FILTERS';
const CLEAR_VENDORS_FILTERS = 'snappFood/vendors/CLEAR_VENDORS_FILTERS';

export const addVendorsList = createAction<{
  list: Array<IVendorItemModel>;
  rowCount: number | undefined;
}>(ADD_VENDORS_LIST);
export const clearVendorsList = createAction(CLEAR_VENDORS_LIST);

export const addSelectedFilter = createAction<string>(ADD_SELECTED_FILTER);
export const removeSelectedFilter = createAction<string>(
  REMOVE_SELECTED_FILTER,
);

export const addVendorsFilters =
  createAction<Array<IFilterItemModel>>(ADD_VENDORS_FILTERS);
export const clearVendorsFilters = createAction(CLEAR_VENDORS_FILTERS);

interface IStateModel {
  vendorsList: {
    list: Array<IVendorItemModel>;
    rowCount: number | undefined;
  };
  selectedFilters: Array<string>;
  vendorsFilters: Array<IFilterItemModel>;
}
const initialState: IStateModel = {
  vendorsList: {
    list: [],
    rowCount: undefined,
  },
  selectedFilters: [],
  vendorsFilters: [],
};

export default createReducer<IStateModel>(initialState, builder => {
  builder
    .addCase(addVendorsList, (state, action) => {
      state.vendorsList = {
        list: [...state.vendorsList.list, ...action.payload.list],
        rowCount: action.payload.rowCount,
      };
    })
    .addCase(clearVendorsList, state => {
      state.vendorsList = initialState.vendorsList;
    })
    .addCase(addSelectedFilter, (state, action) => {
      state.selectedFilters = [...state.selectedFilters, action.payload];
    })
    .addCase(removeSelectedFilter, (state, action) => {
      state.selectedFilters = state.selectedFilters.filter(
        item => item !== action.payload,
      );
    })
    .addCase(addVendorsFilters, (state, action) => {
      state.vendorsFilters = [...state.vendorsFilters, ...action.payload];
    })
    .addCase(clearVendorsFilters, state => {
      state.vendorsFilters = [];
    });
});
