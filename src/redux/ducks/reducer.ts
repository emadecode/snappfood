import {Action, combineReducers, Reducer} from '@reduxjs/toolkit';

import vendors from './vendors';

const appReducer = combineReducers({
  vendors,
});

const rootReducer: Reducer = (state: RootState, action: Action) => {
  if (action.type === 'banili/general/USER_LOGOUT') {
    return appReducer(undefined, action);
  }
  return appReducer(state, action);
};

export type RootState = ReturnType<typeof appReducer>;

export default rootReducer;
