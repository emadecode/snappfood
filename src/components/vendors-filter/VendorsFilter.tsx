import React from 'react';
import {useSelector} from 'react-redux';
import {RootState} from '../../redux/ducks/reducer';

import FilterItem from '../filter-item/FilterItem';
import {IFilterItemModel} from './../../areas/vendors-list-page/VendorsListPage.models';
import styles from './VendorsFilter.module.scss';

const VendorsFilter = () => {
  const vendorsFilters = useSelector(
    (state: RootState) => state.vendors.vendorsFilters,
  );

  const renderFiltersList = (data: Array<IFilterItemModel>) => {
    return data.map((item, idx) => <FilterItem key={idx} itemData={item} />);
  };

  return (
    <div className={styles.vendorFilters} data-cy="vendorsFilter">
      {renderFiltersList(vendorsFilters)}
    </div>
  );
};

export default VendorsFilter;
