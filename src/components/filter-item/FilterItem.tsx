import React, {FC, useState} from 'react';
import {IFilterItemModel} from '../../areas/vendors-list-page/VendorsListPage.models';
import styles from './FilterItem.module.scss';
import {useDispatch} from 'react-redux';
import {
  addSelectedFilter,
  removeSelectedFilter,
} from '../../redux/ducks/vendors';

interface IFilterItemProps {
  itemData: IFilterItemModel;
}

const FilterItem: FC<IFilterItemProps> = ({itemData}) => {
  const [isSelected, setIsSelected] = useState<boolean>(false);
  const dispatch = useDispatch();
  const handleFilterClick = () => {
    setIsSelected(value => !value);
    const shouldAdd = !isSelected;
    shouldAdd
      ? dispatch(addSelectedFilter(itemData.value))
      : dispatch(removeSelectedFilter(itemData.value));
  };

  return (
    <div
      className={
        styles.filterItem + ' ' + (isSelected && styles.filterItem__isActive)
      }
      onClick={handleFilterClick}
      data-cy="filterItem"
    >
      {itemData.title}
    </div>
  );
};

export default FilterItem;
