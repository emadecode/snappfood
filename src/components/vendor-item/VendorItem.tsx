import React, {FC} from 'react';
import styles from './VendorItem.module.scss';
import {IVendorItemModel} from './../../areas/vendors-list-page/VendorsListPage.models';
import NumberFormat from 'react-number-format';
import {StarFill} from '@styled-icons/bootstrap';

interface IVendorItemProps {
  vendorItem: IVendorItemModel;
  style: any;
}

const VendorItem: FC<IVendorItemProps> = ({vendorItem, style}) => {
  const {data} = vendorItem;
  return (
    <div
      style={style}
      className={styles.vendorItemContainer}
      data-cy="vendorItem"
    >
      <div className={styles.vendorItem}>
        <div className={styles.vendorItem__header}>
          <div className={styles.vendorItem__imageContainer}>
            <img
              className={styles.vendorItem__image}
              src={data.backgroundImage}
              alt=""
            />
          </div>
          <div className={styles.vendorItem__logoContainer}>
            <img
              className={styles.vendorItem__logo}
              src={data.defLogo}
              alt=""
            />
          </div>
        </div>
        <div className={styles.vendorItem__body}>
          <h4 className={styles.vendorItem__title}>{data.title}</h4>
          <h4 className={styles.vendorItemRate}>
            <span className={styles.vendorItemRate__totalVote}>
              ({data.voteCount})
            </span>
            <span className={styles.vendorItemRate__vote}>
              <span className={styles.vendorItemRate__number}>{data.rate}</span>
              <StarFill size={12} />
            </span>
          </h4>
        </div>
        <div className={styles.vendorItem__footer}>
          <h5 className={styles.vendorItem__desc}>{data.description}</h5>
          <div className={styles.delivery}>
            <span className={styles.delivery__type}>
              {data.isZFExpress ? 'ارسال اکسپرس:' : 'پیک فروشنده'}
            </span>
            <span className={styles.delivery__fee}>
              <NumberFormat
                value={data.deliveryFee}
                thousandSeparator={true}
                displayType="text"
              />
              تومان
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VendorItem;
