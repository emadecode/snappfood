import React, {FC, useRef, useState} from 'react';
import {useSelector} from 'react-redux';
import {
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
  Index,
  InfiniteLoader,
  List,
} from 'react-virtualized';
import {RootState} from '../../redux/ducks/reducer';
import VendorItem from '../vendor-item/VendorItem';
import styles from './VendorsList.module.scss';

interface IVendorsListProps {
  onPaginate: (pageNumber: number) => Promise<void>;
}

const VendorsList: FC<IVendorsListProps> = ({onPaginate}) => {
  const [pageNumber, setPageNumber] = useState<number>(0);
  const vendorItemCache = useRef(
    new CellMeasurerCache({
      fixedWidth: true,
      defaultHeight: 500,
    }),
  );

  const vendorsList = useSelector(
    (state: RootState) => state.vendors.vendorsList,
  );

  const isRowLoaded = ({index}: Index) => {
    return !!vendorsList.list[index];
  };

  const loadMoreRows = () => {
    setPageNumber(value => value + 1);
    return onPaginate(pageNumber + 1);
  };

  return (
    <section className={styles.vendorsList} data-cy="vendorsList">
      <InfiniteLoader
        isRowLoaded={isRowLoaded}
        loadMoreRows={loadMoreRows}
        rowCount={vendorsList.rowCount}
        threshold={5}
      >
        {({onRowsRendered, registerChild}) => (
          <AutoSizer>
            {({height, width}) => (
              <List
                className="test"
                height={height}
                width={width}
                rowHeight={vendorItemCache.current.rowHeight}
                deferredMeasurementCache={vendorItemCache.current}
                rowCount={vendorsList.list.length}
                ref={registerChild}
                onRowsRendered={onRowsRendered}
                rowRenderer={({key, index, style, parent}) => {
                  return (
                    <CellMeasurer
                      key={key}
                      cache={vendorItemCache.current}
                      parent={parent}
                      columnIndex={0}
                      rowIndex={index}
                    >
                      <VendorItem
                        vendorItem={vendorsList.list[index]}
                        style={style}
                      />
                    </CellMeasurer>
                  );
                }}
              />
            )}
          </AutoSizer>
        )}
      </InfiniteLoader>
    </section>
  );
};

export default VendorsList;
