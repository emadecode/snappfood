import React, {lazy} from 'react';
import {useRoutes} from 'react-router-dom';
import CustomSuspense from './shared/custom-suspense/CustomSuspense';

const HomePage = lazy(() => import('./areas/home-page/HomePage'));
const VendorsListPage = lazy(
  () => import('./areas/vendors-list-page/VendorsListPage'),
);
const NotFoundPage = lazy(() => import('./areas/not-found-page/NotFoundPage'));

const routes = [
  {
    path: '/',
    element: <CustomSuspense children={<HomePage />} />,
  },
  {
    path: '/restaurant',
    element: <CustomSuspense children={<VendorsListPage />} />,
  },
  {
    path: '*',
    element: <CustomSuspense children={<NotFoundPage />} />,
  },
];

const AppRoutes = () => {
  let rootRoutes = useRoutes(routes);
  return rootRoutes;
};

export default AppRoutes;
