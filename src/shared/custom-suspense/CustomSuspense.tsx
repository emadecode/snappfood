import React, { FC, Suspense } from "react";

interface ICustomSuspenseProps {
  children: React.ReactChild;
}

const CustomSuspense: FC<ICustomSuspenseProps> = ({ children }) => {
  return <Suspense fallback={<div>Loading ...</div>}>{children}</Suspense>;
};

export default CustomSuspense;
