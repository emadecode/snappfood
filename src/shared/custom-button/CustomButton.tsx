import React, {FC} from 'react';
import styled from 'styled-components';
import {mixins} from '../../styles/mixins';
import {IStyledThemeProps} from '../../styles/theme';

interface ICustomButtonStyledProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement>,
    IStyledThemeProps {
  variant?: 'Primary' | 'Secondary';
  width?: 'Normal' | 'Full';
  isGradient?: boolean;
  isRounded?: boolean;
}

const CustomButtonStyled = styled.button<ICustomButtonStyledProps>`
  border-radius: ${({isRounded = true}) => (isRounded ? '4rem' : '0.2rem')};
  border: none;
  padding: 0.6rem 2rem;
  cursor: pointer;
  font-size: 1.1rem;
  background-color: ${({variant, theme}) =>
    variant === 'Secondary'
      ? theme.palette.secondary.main
      : theme.palette.primary.main};
  ${({isGradient = true, theme}) => isGradient && mixins.bgGradient(theme)};
  color: #ffffff;
  width: ${({width}) => (width === 'Full' ? '100%' : 'auto')};
  line-height: 1.6;
`;

interface ICustomButtonProps extends ICustomButtonStyledProps {
  children: React.ReactNode;
}

const CustomButton: FC<ICustomButtonProps> = ({children, ...rest}) => {
  return <CustomButtonStyled {...rest}>{children}</CustomButtonStyled>;
};

export default CustomButton;
