import React, {FC} from 'react';

interface INumberFormatProps {
  value: number;
  prefix: string;
  thousandSeparator: boolean;
}

const NumberFormat: FC<INumberFormatProps> = ({value, prefix}) => {
  return (
    <>
      {value.toFixed(3)} {prefix}
    </>
  );
};

export default NumberFormat;
