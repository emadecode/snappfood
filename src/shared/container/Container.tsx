import React, {FC} from 'react';
import {IStyledThemeProps} from '../../styles/theme';
import styled from 'styled-components';

interface ICustomButtonStyledProps
  extends React.HTMLAttributes<HTMLDivElement>,
    IStyledThemeProps {}

const ContainerStyled = styled.div<ICustomButtonStyledProps>`
  padding-right: 1.5rem;
  padding-left: 1.5rem;
  margin-right: auto;
  margin-left: auto;
`;

const Container: FC<ICustomButtonStyledProps> = ({children, ...rest}) => {
  return <ContainerStyled {...rest}>{children}</ContainerStyled>;
};

export default Container;
