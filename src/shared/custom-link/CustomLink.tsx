import React, {FC} from 'react';
import styled from 'styled-components';
import {mixins} from '../../styles/mixins';
import {IStyledThemeProps} from '../../styles/theme';
import {Link, LinkProps} from 'react-router-dom';

interface ICustomLinkStyledProps extends LinkProps, IStyledThemeProps {
  variant?: 'Primary' | 'Secondary';
  width?: 'Normal' | 'Full';
  isGradient?: boolean;
  isRounded?: boolean;
}

const CustomLinkStyled = styled(Link)<ICustomLinkStyledProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: ${({isRounded = true}) => (isRounded ? '4rem' : '0.2rem')};
  border: none;
  padding: 0.6rem 2rem;
  cursor: pointer;
  font-size: 1.1rem;
  background-color: ${({variant, theme}) =>
    variant === 'Secondary'
      ? theme.palette.secondary.main
      : theme.palette.primary.main};
  ${({isGradient = true, theme}) => isGradient && mixins.bgGradient(theme)};
  color: #ffffff;
  width: ${({width}) => (width === 'Full' ? '100%' : 'auto')};
  line-height: 1.6;
  text-decoration: none;
`;

interface ICustomLinkProps extends ICustomLinkStyledProps {
  children: React.ReactNode;
}

const CustomLink: FC<ICustomLinkProps> = ({children, ...rest}) => {
  return <CustomLinkStyled {...rest}>{children}</CustomLinkStyled>;
};

export default CustomLink;
