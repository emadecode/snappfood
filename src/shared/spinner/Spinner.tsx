import React from 'react';
import styled from 'styled-components';
import {IStyledThemeProps} from '../../styles/theme';

interface IStyledSpinnerPropsModel
  extends React.HtmlHTMLAttributes<HTMLDivElement>,
    IStyledThemeProps {
  width?: number;
  isWhite?: boolean;
}

const Spinner = styled.div<IStyledSpinnerPropsModel>`
  display: inline-block;

  :after {
    content: ' ';
    display: block;
    width: ${({width}) => (width ? width + 'rem' : '1.4rem')};
    height: ${({width}) => (width ? width + 'rem' : '1.4rem')};
    margin: 0 0.2rem;
    border-radius: 50%;
    border: 0.2rem solid
      ${({isWhite = true, theme}) =>
        isWhite ? '#fff' : theme.palette.primary.main};
    border-color: ${({isWhite = true, theme}) =>
        isWhite ? '#fff' : theme.palette.primary.main}
      transparent
      ${({isWhite = true, theme}) =>
        isWhite ? '#fff' : theme.palette.primary.main}
      transparent;
    animation: spinnerAnimation 1.2s linear infinite;
  }
  @keyframes spinnerAnimation {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

export default Spinner;
