export interface IVendorsListResultModel {
  count: number;
  finalResult: Array<IResultItemType>;
  extra_sections: IExtraSectionsModel;
}

export interface IExtraSectionsModel {
  categories: Categories;
  filters: Filters;
}

type IResultItemType = IVendorItemModel | ITextItemTypeModel;

export interface ITextItemTypeModel {
  type: 'TEXT';
  data: string;
}

export interface IVendorItemModel {
  type: 'VENDOR';
  data: IVendorItemTypeDataModel;
}

export interface Schedule {
  type: number;
  weekday: number;
  allDay: boolean;
  startHour: string;
  stopHour: string;
}

export interface Badge {
  title: string;
  description: string;
  image: string;
  white_image: string;
}

export interface CuisinesArray {
  id: number;
  title: string;
}

export interface UserImage {
  id: number;
  description?: any;
  fileName: string;
  thumbNailSource: string;
  likeCount: number;
  commentCount: number;
  timeDifference: number;
  imageType: string;
  userType: string;
}

export interface IVendorItemTypeDataModel {
  id: number;
  vendorCode: string;
  noOrder: boolean;
  title: string;
  description: string;
  rate: number;
  rating: number;
  logo: string;
  defLogo: string;
  taxEnabled: boolean;
  taxIncluded: boolean;
  taxEnabledInProducts: boolean;
  taxEnabledInPackaging: boolean;
  taxEnabledInDeliveryFee: boolean;
  tax: number;
  serviceFee: number;
  deliveryArea: string;
  discount: number;
  isOpen: boolean;
  minDeliveryFee: number;
  maxDeliveryFee: number;
  deliveryTime: number;
  paymentTypes: number[];
  schedules: Schedule[];
  minOrder: number;
  address: string;
  phone: string;
  website: string;
  status: number;
  lat: number;
  lon: number;
  restaurantClass: string;
  foodTypes: any[];
  restaurantTypes: any[];
  isFavorite: boolean;
  priority: number;
  city: string;
  area: string;
  commentCount: number;
  recommendedFor: string;
  establishment: string;
  mostPopularItems: string;
  costsForTwo: number;
  onlineOrder: boolean;
  voteCount: number;
  discountType?: any;
  menuUrl: string;
  discountValue: number;
  discountForAll: boolean;
  containerFee: number;
  deliveryGuarantee: boolean;
  badges: Badge[];
  discountStartHour1: string;
  discountStopHour1: string;
  discountStartHour2: string;
  discountStopHour2: string;
  discountValueForView: number;
  coverPath: string;
  cuisinesArray: CuisinesArray[];
  preOrderEnabled: boolean;
  vendorType: string;
  childType: string;
  budgetClass: string;
  vendorTypeTitle: string;
  isZFExpress: boolean;
  deliveryFee: number;
  backgroundImage: string;
  backgroundImageCustom: string;
  has_coupon: boolean;
  coupon_count: number;
  best_coupon: string;
  userImage: UserImage[];
  menuImage: any[];
  countReview: number;
  countOfUserImages: number;
  deliveryFeeDiscount: number;
  trendingScore: number;
  delay: string;
  deliver: boolean;
  eta: number;
  min_eta: number;
  max_eta: number;
  open_at_eta: boolean;
  action: string;
  has_delay: boolean;
  delay_time: number;
  total_time: number;
  bid: boolean;
  superTypeAlias: string;
  is_food_party: boolean;
  is_market_party: boolean;
  click_id?: any;
  cpc_campaign_hash?: any;
  is_ecommerce: boolean;
  is_economical: boolean;
  is_grocery_vip: boolean;
  is_grocery_returnable: boolean;
  is_grocery_economic: boolean;
}

export interface Categories {
  single_choice: boolean;
  data: any[];
}

export interface IFilterItemModel {
  image?: any;
  title: string;
  value: string;
  single_choice: boolean;
  selected: boolean;
  kind: string;
  suggest: boolean;
}
export interface Section {
  section_name: string;
  section_name_fa: string;
  data: IFilterItemModel[];
}
export interface Datum2 {
  image?: any;
  title: string;
  value: string;
  single_choice: boolean;
  selected: boolean;
  kind: string;
}
export interface Top {
  data: Datum2[];
}
export interface Filters {
  sections: Section[];
  top: Top;
}
