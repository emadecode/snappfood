import React, {useCallback, useEffect, useRef} from 'react';
import {isVendorItem} from '../../core/typeGuards';
import useQuery from '../../hooks/useQuery';
import {IVendorsListResultModel} from './VendorsListPage.models';
import styles from './VendorsListPage.module.scss';
import {useDispatch, useSelector} from 'react-redux';
import {
  addVendorsFilters,
  addVendorsList,
  clearVendorsFilters,
  clearVendorsList,
} from './../../redux/ducks/vendors';
import VendorsFilter from '../../components/vendors-filter/VendorsFilter';
import Container from './../../shared/container/Container';
import {RootState} from '../../redux/ducks/reducer';
import Spinner from './../../shared/spinner/Spinner';
import VendorsList from '../../components/vendors-list/VendorsList';

const VendorsListPage = () => {
  const dispatch = useDispatch();
  const didMount = useRef(false);
  const hasFiltersList = useRef(false);
  const selectedFilters = useSelector(
    (state: RootState) => state.vendors.selectedFilters,
  );

  const filterVendorsList = useCallback(
    (data: IVendorsListResultModel) => data.finalResult.filter(isVendorItem),
    [],
  );

  const {sendRequest, isLoading} = useQuery<IVendorsListResultModel>({
    url: '/restaurant/vendors-list',
    filters: selectedFilters,
    callback: {
      onSuccess: response => {
        if (!hasFiltersList.current) {
          dispatch(
            addVendorsFilters(
              response.data.extra_sections.filters.sections[0].data,
            ),
          );
          hasFiltersList.current = true;
        }
        dispatch(
          addVendorsList({
            list: filterVendorsList(response.data),
            rowCount: response.data.count,
          }),
        );
      },
    },
  });

  useEffect(() => {
    if (didMount.current) {
      dispatch(clearVendorsList());
      sendRequest(0);
    } else didMount.current = true;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedFilters]);

  useEffect(() => {
    return () => {
      dispatch(clearVendorsFilters());
      dispatch(clearVendorsList());
    };
  }, [dispatch]);

  return (
    <main className={styles.vendorsPage}>
      <Container>
        <section>
          <VendorsFilter />
        </section>
      </Container>
      <VendorsList onPaginate={sendRequest} />
      {isLoading ? (
        <div className={styles.vendorsListLoading}>
          <Spinner />
        </div>
      ) : (
        ''
      )}
    </main>
  );
};

export default VendorsListPage;
