import React from 'react';
import CustomLink from '../../shared/custom-link/CustomLink';
import styles from './HomePage.module.scss';
import {ShopWindow} from '@styled-icons/bootstrap';
const HomePage = () => {
  return (
    <main className={styles.homePage}>
      <section className={styles.welcomeSection}>
        <div className={styles.welcomeSection__header} data-cy="welcomeText">
          به اسنپ‌فود خوش آمدید
        </div>
        <div className={styles.welcomeSection__body}>
          <CustomLink
            to={{
              pathname: '/restaurant',
            }}
            data-cy="vendorsListButton"
          >
            <ShopWindow size={20} className={styles.welcomeButtonIcon} />
            لیست رستوران‌ها
          </CustomLink>
        </div>
      </section>
    </main>
  );
};

export default HomePage;
