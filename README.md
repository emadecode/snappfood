# SnappFood Test

Test task for SnappFood using react-js and styled-components.
State management system: Redux
Test tool : Cypress
Host(continuous development) tool: Vercel.com

## Url

https://snappfood.vercel.app/

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm cy`

Opens the test runner in the visual mode.

### `npm cy`

Opens the test runner in the visual and production mode.

### `npm cy:test`

Launches the test runner in the interactive watch mode.

### `npm cy:test:prod`

Launches the test runner in the interactive watch and production mode.

### `npm cy:gr`

Generates test Results in a visual mode.(HTML)
**Note: Don't forget to run `npm cy:test` before that**
