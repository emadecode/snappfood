/// <reference types="cypress" />

describe('Test Home Page - load', () => {
    beforeEach(() => {
        cy.visit(Cypress.env("homePage_url"));
    });
    it('should be contain welcome text', () => {
        cy.get("[data-cy=welcomeText]").should('be.visible');
    });
    it('should be contain vendors list button', () => {
        cy.get("[data-cy=vendorsListButton]").should('be.visible');
    });
});

describe('Test Login Page - Vendors list button', () => {
    beforeEach(() => {
        cy.visit(Cypress.env("homePage_url"));
    });
    it('should navigate to restaurant page on click', () => {
        cy.get("[data-cy=vendorsListButton]").click().then(() => {
            cy.location('pathname').should('contain', Cypress.env("restaurantPage_url"));
        });
    });
});