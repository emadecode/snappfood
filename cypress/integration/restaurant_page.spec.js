/// <reference types="cypress" />

describe('Test Restaurant Page - load', () => {
    let vendorsList;
    let filtersList;
    beforeEach(() => {
        cy.intercept('GET', '/mobile/v3/restaurant/**').as('getVendorsList');
        cy.visit(Cypress.env("restaurantPage_url"));
        cy.wait('@getVendorsList').then(res => {
            const finalResult = res.response.body.data.finalResult;
            vendorsList = finalResult.filter(item => item.type === "VENDOR")
            filtersList = res.response.body.data.extra_sections.filters.sections[0].data;
        })
    });
    it('should load vendors filter', () => {
        cy.get("[data-cy=vendorsFilter]").should('be.visible');
    });
    it('should load filter items based on result length', () => {
        cy.get("[data-cy=filterItem]").should('have.length', filtersList.length);
    });
    it('should load vendor items based on result length', () => {
        cy.get("[data-cy=vendorItem]").should('have.length', vendorsList.length);
    });
});

describe('Test Restaurant Page - Filters', () => {
    beforeEach(() => {
        cy.intercept('GET', '/mobile/v3/restaurant/**').as('getVendorsList');
        cy.visit(Cypress.env("restaurantPage_url"));
    });
    it('should filter result on filter item click', () => {
        let vendorsList;
        cy.get("[data-cy=filterItem]").first().click();
        cy.wait('@getVendorsList').then(res => {
            const finalResult = res.response.body.data.finalResult;
            vendorsList = finalResult.filter(item => item.type === "VENDOR")
        }).then(() => {
            cy.get("[data-cy=vendorItem]").should('have.length', vendorsList.length);
        })

    });
    it("should load data on scroll", () => {
        cy.get('[data-cy=vendorsList] .ReactVirtualized__List').scrollTo('bottom');
        cy.wait('@getVendorsList').its('response.body.status').should("eq", true)
    })
});
